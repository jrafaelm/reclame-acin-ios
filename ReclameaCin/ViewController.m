//
//  ViewController.m
//  ReclameaCin
//
//  Created by Jose Rafael Moraes Garcia da Rocha on 7/31/13.
//

#import "ViewController.h"
#import "Constants.h"
#import "AFJSONRequestOperation.h"
#import "MBProgressHUD.h"
#import "Issue.h"
#import "IssuesDetailViewController.h"
#import "User.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    _tableData = [NSMutableArray array];
    
    _users = [NSMutableArray array];
    [self loadUsers];
    
}

-(void)viewWillAppear:(BOOL)animated{

    [_tableData removeAllObjects];
    [self loadIssues];
}

-(void) loadIssues{
    
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:kBaseURL kIssuesPath]];
    NSURLRequest * urlRequest = [NSURLRequest requestWithURL:url];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:urlRequest success:^(NSURLRequest * request, NSHTTPURLResponse * response, id json){
        if (response.statusCode == 200) {
            if ([json isKindOfClass:[NSArray class]]) {
                for (NSDictionary* issueDic in json) {
                    Issue * issue = [[Issue alloc] initWithDictionary:issueDic];
                    [_tableData addObject:issue];
                }
            }else if ([json isKindOfClass:[NSDictionary class]]){
                Issue * issue = [[Issue alloc] initWithDictionary:json];
                [_tableData addObject:issue];
            }else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção" message:@"Ocorreu um erro, tente novamente mais tarde." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
            [_tableView reloadData];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção" message:@"Ocorreu um erro, tente novamente mais tarde." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError* error, id json){
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção" message:@"Ocorreu um erro, tente novamente mais tarde." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }];
    
    [operation start];
}
-(void) loadUsers{
    
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:kBaseURL kUsersPath]];
    NSURLRequest * urlRequest = [NSURLRequest requestWithURL:url];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:urlRequest success:^(NSURLRequest * request, NSHTTPURLResponse * response, id json){
        if (response.statusCode == 200) {
            if ([json isKindOfClass:[NSArray class]]) {
                for (NSDictionary* userDic in json) {
                    User * user = [[User alloc] initWithDictionary:userDic];
                    [_users addObject:user];
                }
            }else if ([json isKindOfClass:[NSDictionary class]]){
                User * user = [[User alloc] initWithDictionary:json];
                [_users addObject:user];
            }else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção" message:@"Ocorreu um erro, tente novamente mais tarde." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
            [_tableView reloadData];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção" message:@"Ocorreu um erro, tente novamente mais tarde." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError* error, id json){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção" message:@"Ocorreu um erro, tente novamente mais tarde." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }];
    
    [operation start];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _tableData.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString* cellId = @"IssueCellId";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellId];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    Issue *item = _tableData [indexPath.row];
    cell.textLabel.text = item.title;
    cell.detailTextLabel.text = item.desc;
    return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Issue* issue=(Issue*) _tableData[indexPath.row];
    IssuesDetailViewController * vc = [[IssuesDetailViewController alloc] init];
    vc.users = _users;
    vc.issue = issue;
    vc.title = @"Detalhes";
    [self.navigationController pushViewController:vc animated:YES];
}
@end
