//
//  Issue.h
//  ReclameaCin
//
//  Created by Jose Rafael Moraes Garcia da Rocha on 8/5/13.
//

#import <Foundation/Foundation.h>

@interface Issue : NSObject
@property (strong,nonatomic) NSString *identifier;
@property (strong,nonatomic) NSString *authorId;
@property (strong,nonatomic) NSString *title;
@property (strong,nonatomic) NSString *desc;
- (id)initWithDictionary:(NSDictionary *) dictionary;

@end
