//
//  IssuesDetailViewController.m
//  ReclameaCin
//
//  Created by Rafael Moraes on 8/11/13.
//

#import "IssuesDetailViewController.h"
#import "User.h"
#import "AFJSONRequestOperation.h"
#import "MBProgressHUD.h"
#import "Constants.h"
@interface IssuesDetailViewController ()

@end

@implementation IssuesDetailViewController{
    NSMutableArray * _positiveVotes;
    NSMutableArray * _negativeVotes;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self loadIssueWithID:self.issue.identifier];
    [self setUpView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return  _positiveVotes.count;
    }else if (section == 1){
        return _negativeVotes.count;
    }else{
        return 0;
    }
    
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 2;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    User * user;
    
    NSString* cellId = @"UserCellId";
    
    if (indexPath.section == 0) {
        user = _positiveVotes[indexPath.row];
    }else if (indexPath.section == 1){
        user = _negativeVotes[indexPath.row];
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    cell.textLabel.text = user.login;
    return cell;
    
    
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return @"Usuários que concordam";
    }else if (section == 1){
        
        return @"Usuários que discordam";
    }else{
        return @"";
    }
}

-(void) loadIssueWithID:(NSString*) identifier{
    
    NSString *issuePath = [NSString stringWithFormat:kIssuesDetailPath,identifier];
    NSURL* url = [NSURL URLWithString: [NSString stringWithFormat:@"%@%@", kBaseURL,issuePath]];
    NSURLRequest * urlRequest = [NSURLRequest requestWithURL:url];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:urlRequest success:^(NSURLRequest * request, NSHTTPURLResponse * response, id json){
        if (response.statusCode == 200) {
            if ([json isKindOfClass:[NSArray class]]) {
                NSArray* jsonArray = json;
                if (jsonArray.count > 1) {
                    NSDictionary * votes = jsonArray[1];
                    NSString* positiveVotes = [votes valueForKey:@"aprovados"];
                    NSString* negativeVotes = [votes valueForKey:@"reprovados"];
                    _positiveVotes = [self parseUsersArray:positiveVotes];
                    _negativeVotes = [self parseUsersArray:negativeVotes];
                    [_tableView reloadData];
                }
            }else if ([json isKindOfClass:[NSDictionary class]]){
#warning parse 
                NSLog(@"Apenas dictionary");
            }else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção" message:@"Ocorreu um erro, tente novamente mais tarde." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
            [_tableView reloadData];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção" message:@"Ocorreu um erro, tente novamente mais tarde." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError* error, id json){
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção" message:@"Ocorreu um erro, tente novamente mais tarde." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }];
    
    [operation start];
}

-(void) setUpView{
    for (User *user in _users) {
        if([user.identifier isEqualToString:_issue.authorId]){
            _lblAuthor.text = user.login;
            break;
        }
    }
    
    _lblTitle.text = _issue.title;
    _tvDescription.text = _issue.desc;
    
    NSMutableArray *newItems = [self.toolbarItems mutableCopy];
    UIBarButtonItem *share = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(share:)];

    [newItems addObject:share];
    self.navigationController.toolbarItems = newItems;
}

- (NSMutableArray *) parseUsersArray:(NSString *) usersJsonStr{
    NSMutableArray* users = [NSMutableArray array];
    NSArray* decodedArray;
    if (usersJsonStr) {
        NSError * error;
        NSData *data=[usersJsonStr dataUsingEncoding:NSUTF8StringEncoding];
        NSArray *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions    error:&error];
        decodedArray = jsonResponse;
    }
    if (decodedArray && decodedArray.count > 0) {
    for (NSDictionary* userDic in decodedArray) {
            User* user;
            NSString* userId = [[userDic valueForKey:@"usuario_id"]description];
            for (User *userLoaded in _users) {
                if([userLoaded.identifier isEqualToString:userId]){
                    user = userLoaded;
                    break;
                }
            }
            [users addObject:user];
        }
    }
    return users;
}

-(NSString*)getEmailText{
    
    NSMutableString* text = [NSMutableString string];
    [text appendFormat:@"Título: %@ \n\n",self.issue.title];
    
    [text appendFormat:@"Descrição: %@ \n\n",self.issue.desc];
    
    [text appendFormat:@"Autor: %@ \n\n",_lblAuthor.text];
    
    if (_positiveVotes && _positiveVotes.count > 0) {
        
        [text appendFormat:@"Usuários que Concordam:\n"];
        for (User* user in _positiveVotes) {
            [text appendFormat:@"%@, %@\n",user.login,user.email];
        }
        
    }

    if (_negativeVotes && _negativeVotes.count > 0) {
        
        
        [text appendFormat:@"Usuários que Discordam:\n"];
        for (User* user in _negativeVotes) {
            [text appendFormat:@"%@, %@\n",user.login,user.email];
        }
    }
    
    NSString * result = [NSString stringWithString:text];
    return result;
    
    
}

-(IBAction)btnEmailTapped:(id)sender{
    [self displayComposerSheet];
}

-(void)displayComposerSheet
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    
    [picker setSubject:@"Relatório Reclame aCIn"];
    
    // Fill out the email body text.
    NSString *emailBody = [self getEmailText];
    [picker setMessageBody:emailBody isHTML:NO];
    
    picker.mailComposeDelegate = self;
    // Present the mail composition interface.
    [self presentViewController:picker animated:YES completion:^{}];
}

// The mail compose view controller delegate method
- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}
@end
