//
//  AppDelegate.h
//  ReclameaCin
//
//  Created by Jose Rafael Moraes Garcia da Rocha on 7/31/13.
//  Copyright (c) 2013 Mixtecnologia LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *navController;

@end
