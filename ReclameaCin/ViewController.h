//
//  ViewController.h
//  ReclameaCin
//
//  Created by Jose Rafael Moraes Garcia da Rocha on 7/31/13.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    __weak IBOutlet UITableView * _tableView;
    NSMutableArray* _tableData;
    NSMutableArray* _users;
}

@end
