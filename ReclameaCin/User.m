//
//  User.m
//  ReclameaCin
//
//  Created by Rafael Moraes on 8/11/13.
//

#import "User.h"

@implementation User

- (id)initWithDictionary:(NSDictionary *) dictionary;
{
    self = [super init];
    if (self) {
        self.login = [dictionary valueForKey:@"login"];
        self.email = [dictionary valueForKey:@"email"];
        self.identifier = [[dictionary valueForKey:@"id"] description];
    }
    return self;
}

@end
