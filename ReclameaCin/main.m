//
//  main.m
//  ReclameaCin
//
//  Created by Jose Rafael Moraes Garcia da Rocha on 7/31/13.
//  Copyright (c) 2013 Mixtecnologia LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
