//
//  Issue.m
//  ReclameaCin
//
//  Created by Jose Rafael Moraes Garcia da Rocha on 8/5/13.
//

#import "Issue.h"

@implementation Issue

- (id)initWithDictionary:(NSDictionary *) dictionary;
{
    self = [super init];
    if (self) {
        self.title = [dictionary valueForKey:@"titulo"];
        self.desc = [dictionary valueForKey:@"descricao"];
        self.identifier = [[dictionary valueForKey:@"id"] description];
        self.authorId = [[dictionary valueForKey:@"usuario_id"] description];
    }
    return self;
}


@end
