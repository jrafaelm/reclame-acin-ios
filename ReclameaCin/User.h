//
//  User.h
//  ReclameaCin
//
//  Created by Rafael Moraes on 8/11/13.
//  Copyright (c) 2013 Mixtecnologia LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
@property (strong,nonatomic) NSString* identifier;
@property (strong,nonatomic) NSString* login;
@property (strong,nonatomic) NSString* email;


- (id)initWithDictionary:(NSDictionary *) dictionary;
@end
