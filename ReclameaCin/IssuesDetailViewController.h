//
//  IssuesDetailViewController.h
//  ReclameaCin
//
//  Created by Rafael Moraes on 8/11/13.
//

#import <UIKit/UIKit.h>
#import "Issue.h"
#import <MessageUI/MessageUI.h>
@interface IssuesDetailViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate>{

    __weak IBOutlet UILabel * _lblAuthor;
    __weak IBOutlet UILabel * _lblTitle;
    __weak IBOutlet UITextView * _tvDescription;
    __weak IBOutlet UITableView * _tableView;

}

@property (nonatomic,strong) NSArray* users;
@property (nonatomic,strong) Issue* issue;

@end
